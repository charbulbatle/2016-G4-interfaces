package org.charbulbatle.rankit.domain

import java.time.LocalDateTime
import org.uqbar.commons.utils.Observable
import org.eclipse.xtend.lib.annotations.Accessors
import org.charbulbatle.rankit.domain.repos.UserRepo

@Accessors
@Observable
class Qualification {
	
	String evaluado
	Integer puntos
	LocalDateTime fecha
	String user
	String detalle
	Boolean tieneContenidoOfensivo
	
	new(){}
	
	new(String calificado, LocalDateTime fecha, Integer puntos, Boolean ofensiva, String username, String detalle){
		this.evaluado = calificado
		this.fecha = fecha
		this.puntos = puntos
		this.tieneContenidoOfensivo = ofensiva
		this.user = username
		this.detalle = detalle
	}
	def void setTieneContenidoOfensivo(Boolean bool){
		this.tieneContenidoOfensivo = bool
	}
}