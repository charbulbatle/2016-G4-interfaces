package org.charbulbatle.rankit.domain.repos

import java.time.LocalDateTime
import java.util.List
import org.charbulbatle.rankit.domain.Qualification
import org.eclipse.xtend.lib.annotations.Accessors
import java.util.ArrayList
import java.util.NoSuchElementException
import org.charbulbatle.rankit.domain.Qualifiable
import java.util.stream.Collectors

@Accessors
class QualificationRepo {
    private List<Qualification> calificaciones
    private static QualificationRepo instance

    new(){
    	super()
        this.calificaciones = new ArrayList
    }
    
    def static getInstance(){
		if(instance == null)
			instance = new QualificationRepo()
		return instance
	}
	
	def agregarCalificacion(Qualification calificacion) {
		if(calificacionValida(calificacion) && !existeCalificacion(calificacion)){
			calificaciones.add(calificacion)
			var Qualifiable evaluado = QualifiableRepo.instance.getbyName(calificacion.evaluado)
			evaluado.calificaciones.add(calificacion.puntos)
			return true
		} else{
			return false
		}
	}
	
	def agregarCalificaciones(List<Qualification> calificaciones){
		for(calif : calificaciones){
			agregarCalificacion(calif)
		}
	}
	
	def calificacionValida(Qualification calificacion) {
		return UserRepo.instance.has(calificacion.user) && QualifiableRepo.instance.has(calificacion.evaluado)
	}
	
	def existeCalificacion(Qualification calificacion){
		return getByUserAndQualifiable(calificacion.user,calificacion.evaluado ) != null
	}
	
	def eliminar(Qualification quali) {
		var calificacion = getByUserAndQualifiable(quali.user, quali.evaluado)
		calificaciones.remove(calificacion)
	}
	
	def getCantidadDeCalificaciones(){
		this.calificaciones.size
	}
	def getCantidadDeCalificacionesOfensivas(){
		this.calificaciones.filter[calif | calif.tieneContenidoOfensivo].size
	}
	
	def getCantidadDeCalificacionesNoOfensivas(){
		this.calificaciones.filter[calif | !calif.tieneContenidoOfensivo].size
	}
	
	def Qualification getByUserAndQualifiable(String username, String qualiname){
		calificaciones.findFirst[calificacion | calificacion.evaluado.equals(qualiname) && calificacion.user.equals(username)]
	}
	
	def calificacionesPara(Qualifiable qualifiable) {
		this.calificaciones.stream.filter[cali | cali.evaluado.equals(qualifiable.nombre)].collect(Collectors.toList)
	}
	
	def cantCalificacionesPara(Qualifiable qualifiable){
		calificacionesPara(qualifiable).size
	}
	
	def calificacionPromedioPara(Qualifiable qualifiable) {
		try{calificacionesPara(qualifiable).stream
						   				   .map[cali| cali.puntos]
						   				   .reduce[p1, p2 | p1 + p2 ]
						   				   .get / cantCalificacionesPara(qualifiable)
		} catch(NoSuchElementException e){
			return 0
		}
	}
	
	def getCalificacionesParaUsuario(String username) {
		if(username != null){
			this.calificaciones.filter[calif | calif.user == username].toList
		}else{
			this.calificaciones
		}
		
	}
	
	def reemplazarCalificacion(Qualification qualification) {
		var calif = getByUserAndQualifiable(qualification.user, qualification.evaluado)
		calif => [
			puntos = qualification.puntos
			fecha = qualification.fecha
			detalle = qualification.detalle
			tieneContenidoOfensivo = qualification.tieneContenidoOfensivo
		]
		void
	}
    
}