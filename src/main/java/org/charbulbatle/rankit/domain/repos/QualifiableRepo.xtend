package org.charbulbatle.rankit.domain.repos

import org.charbulbatle.rankit.domain.Qualifiable
import java.util.List
import java.util.ArrayList

/**
 * Muestra juntos los servicios y lugares
 */
class QualifiableRepo {
	private static QualifiableRepo instance
	
	def static List<Qualifiable> getQualifiables(){
		var list = new ArrayList
		list.addAll(PlaceRepo.instance.servicios)
		list.addAll(ServiceRepo.instance.servicios)
		list
	}
	
	def getEnabledQualifiables(){
		var list = new ArrayList
		list.addAll(PlaceRepo.instance.serviciosHabilitados)
		list.addAll(ServiceRepo.instance.serviciosHabilitados)
		list
	}
	
	def getbyName(String name){
		qualifiables.findFirst[qualifiable | qualifiable.nombre.equals(name)]
	}
	
	def static getInstance(){
		if(instance == null)
			instance = new QualifiableRepo()
		return instance
	}
	
	def has(String evaluado) {
		return getQualifiables().exists[elem | elem.nombre.equals(evaluado)]
	}
	
}