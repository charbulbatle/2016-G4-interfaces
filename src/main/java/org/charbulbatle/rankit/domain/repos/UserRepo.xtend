package org.charbulbatle.rankit.domain.repos

import java.time.LocalDateTime
import java.util.List
import org.charbulbatle.rankit.domain.User
import org.eclipse.xtend.lib.annotations.Accessors
import java.util.ArrayList

@Accessors
class UserRepo {
	private List<User> usuarios
	private static UserRepo instance;
	
	new(){
		usuarios = new ArrayList
	}
	
	def static getInstance(){
		if(instance == null)
			instance = new UserRepo()
		
		return instance
	}
	
	def void generarUsuarioNuevo(String nombre){
		val newUser = new User(nombre, "1234", LocalDateTime.now, false, false)
		usuarios.add(newUser)
	}
	
	def void eliminarUsuario(User user) {
		usuarios.remove(user)
	}
	
	def getByName(String name){
		usuarios.findFirst[user | user.nombre.equals(name)]
	}
	
	def getById(Integer id){
		usuarios.findFirst[user | user.id.equals(id)]
	}
	
	def getCantidadDeUsuariosActivos(){
		this.usuarios.filter[user | user.activo].size
	}
	
	def getCantidadDeUsuarios(){
		this.usuarios.size
	}
	def getCantidadDeUsuariosBaneados(){
		this.usuarios.filter[user | user.baneado].size
	}
	def getCantidadDeUsuariosInactivos(){
		this.usuarios.filter[user| !user.activo].size
	}
	
	def nuevoUsuario(User newUser){
		if(has(newUser.nombre)){
			return false
		} else {
			 return this.usuarios.add(newUser)
		}
	}
	
	def eliminarUsuarios(){
	    this.usuarios = new ArrayList
	}
	
	def filtrarPorNombre(String buscado) {
		this.usuarios.filter[user | user.nombre.contains(buscado)].toList
	}
	
	def esMismoUsuario(User user, String nombre, String pass) {
		user.nombre == nombre && user.password == pass
	}
	
	def has(String name) {
		return this.usuarios.exists[user | user.nombre.equals(name)]
	}
	
}