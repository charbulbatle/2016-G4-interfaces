package org.charbulbatle.rankit.domain.repos

import org.charbulbatle.rankit.domain.Qualifiable

class PlaceRepo extends ServiceRepo{
	private static PlaceRepo instance
    
    def static getInstance(){
		if(instance == null)
			instance = new PlaceRepo()
		
		return instance
	}
	
	override generarServicioNuevo(String nombre){
        this.servicios.add(new Qualifiable(nombre, "lugar"))
    }
}