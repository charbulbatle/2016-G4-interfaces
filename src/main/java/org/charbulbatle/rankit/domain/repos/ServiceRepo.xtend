package org.charbulbatle.rankit.domain.repos

import java.util.ArrayList
import java.util.List
import java.util.stream.Collectors
import org.charbulbatle.rankit.domain.Qualifiable
import org.eclipse.xtend.lib.annotations.Accessors
import org.uqbar.commons.model.ObservableUtils

@Accessors
class ServiceRepo {
    private List<Qualifiable> servicios
	private static ServiceRepo instance
	
	def static getInstance(){
		if(instance == null)
			instance = new ServiceRepo()
		
		return instance
	}
	
    new(){
        this.servicios = new ArrayList
    }
    
    def generarServicioNuevo(String nombre){
        this.servicios.add(new Qualifiable(nombre, "servicio"))
    }
    
    def getByName(String name){
    	this.servicios.stream.filter(qualifiable | qualifiable.nombre.equals(name)).findFirst.get
    }
    
    def getCantidadDeInscriptos(){
    	this.servicios.size
    }
    def getCantidadDeInscriptosHabilitados(){
    	this.servicios.filter[serv | serv.habilitado].size
    	
    }
    
    def getCantidadDeInscriptosDeshabilitados(){
    	this.servicios.filter[serv | !serv.habilitado].size
    }
	
	def eliminarServicio(Qualifiable qualifiable) {
		this.servicios.remove(qualifiable)
		ObservableUtils.firePropertyChanged(this, "servicios")
	}

	def getServicios(String buscado){
		if(buscado == null){
            return servicios
        } else {
            return servicios.stream()
                                          .filter(servicio | servicio.nombre.contains(buscado))
                                          .collect(Collectors.toList)
        }
	}
	
	def getServiciosHabilitados(){
		servicios.filter[ servicio | servicio.habilitado].toList
	}
	
}