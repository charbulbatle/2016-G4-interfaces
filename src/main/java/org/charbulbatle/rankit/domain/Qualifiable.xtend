package org.charbulbatle.rankit.domain

import java.time.LocalDateTime
import java.util.List
import java.util.ArrayList
import org.uqbar.commons.utils.Observable
import org.eclipse.xtend.lib.annotations.Accessors
import java.util.stream.Collectors

@Observable
@Accessors
class Qualifiable {
	//Lugares y servicios :D
    LocalDateTime fechaDeRegistro;
    String nombre;
    String tipo;
    int id;
    Boolean habilitado;
    List<Integer> calificaciones
    protected static int lastID
    
    new(String nombre, String tipo){
    	this.fechaDeRegistro 	= LocalDateTime.now()
    	this.nombre 			= nombre
    	this.tipo 				= tipo
    	this.id 				= lastID + 1
    	this.habilitado 		= false
    	this.calificaciones 	= new ArrayList<Integer>()
    	lastID++;
    	
    }
    
    new(String nombre, String tipo, Boolean habilitado){
    	this(nombre, tipo)
    	this.habilitado = habilitado
    }
    
    def getRatingPromedio(){
    	var result = 0
    	if(calificaciones.size > 0){
    		result = calificaciones.stream
    							   .collect(Collectors.summingInt(x | x)) / calificaciones.size()
    	}
    	return result
    }
    def calificar(Integer calificacion){
    	calificaciones.add(calificacion)
    }
}