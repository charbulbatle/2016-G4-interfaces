package org.charbulbatle.rankit.domain

import java.time.LocalDateTime
import org.eclipse.xtend.lib.annotations.Accessors
import org.uqbar.commons.model.ObservableUtils
import org.uqbar.commons.utils.Observable
import java.util.List
import java.util.ArrayList
import com.fasterxml.jackson.annotation.JsonIgnore

@Observable
@Accessors
class User {
	int id;
    String nombre
    String password
    LocalDateTime fechaDeRegistro
    Boolean activo
    Boolean baneado
    List<Qualification> calificaciones
    static int lastId = 0;
    
    new(){
    	this.id = lastId;
    	lastId++;
    	this.fechaDeRegistro = LocalDateTime.now
    	this.activo = true 
    	this.baneado = false
		this.calificaciones = new ArrayList<Qualification>()
    }
    
    new(String nombre, String pass, LocalDateTime registro, Boolean activo, Boolean baneado){
    	this.id = lastId;
    	lastId++;
    	this.nombre = nombre
    	password = pass
    	this.fechaDeRegistro = registro
    	this.activo = activo 
    	this.baneado = baneado
		this.calificaciones = new ArrayList<Qualification>()
    }
    
    
    def void setBaneado(Boolean banned){
    	baneado = banned
    	if(banned){
    		activo = !banned
    	}
    	
    	ObservableUtils.firePropertyChanged(this, "activo")
    	ObservableUtils.firePropertyChanged(this, "noBaneado")
    }
    @JsonIgnore
    def getNoBaneado(){
    	!baneado
    }
    
    def blanquearClave(){
    	password = "1234"
    }
    
    @JsonIgnore
    def getUltimaPublicacion(){
    	if(!calificaciones.isEmpty){
    		var calif = this.calificaciones.minBy[calif |calif.fecha]
    		return calif.fecha
    	}else {
    		null
    	}
    	
    }
    @JsonIgnore
    def getCantidadDeOfensas(){
    	var ofensas = calificaciones.filter[calif| calif.tieneContenidoOfensivo].size
    	if(ofensas > 5){
    		this.setBaneado(true)
    		ObservableUtils.firePropertyChanged(this, "baneado")
    	}
    	return ofensas
    }
    def agregarCalificacion(Qualification calificacion){
    	calificaciones.add(calificacion)
    	ObservableUtils.firePropertyChanged(this, "ultimaPublicacion")
    	ObservableUtils.firePropertyChanged(this, "cantidadDeOfensas")
    }
    def eliminarCalificacion(Qualification calificacion){
    	calificaciones.remove(calificacion)
    	ObservableUtils.firePropertyChanged(this, "ultimaPublicacion")
    	ObservableUtils.firePropertyChanged(this, "cantidadDeOfensas")
    }
    
}
