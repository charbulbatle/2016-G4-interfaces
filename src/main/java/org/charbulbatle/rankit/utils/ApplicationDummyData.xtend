package org.charbulbatle.rankit.utils

import org.charbulbatle.rankit.domain.repos.PlaceRepo
import org.charbulbatle.rankit.domain.repos.ServiceRepo
import org.charbulbatle.rankit.domain.repos.UserRepo
import org.charbulbatle.rankit.domain.repos.QualificationRepo

class ApplicationDummyData {
	def static void dummyEverything(){
		// Llenar con contenido de ejemplo
		PlaceRepo.instance.servicios 	= new PlacesDummyData().datos
		ServiceRepo.instance.servicios 	= new ServicesDummyData().datos
		UserRepo.instance.usuarios 		= new UserDummyData().datos
		QualificationRepo.instance.agregarCalificaciones(new QualificationDummyData().datos)
	}
}