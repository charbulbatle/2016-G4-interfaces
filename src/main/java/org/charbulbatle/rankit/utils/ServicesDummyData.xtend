package org.charbulbatle.rankit.utils

import org.charbulbatle.rankit.domain.Qualifiable
import org.eclipse.xtend.lib.annotations.Accessors
import java.util.List

@Accessors
class ServicesDummyData extends DummyData<Qualifiable> {
	
	override generarDatos() {
		generarServiciosConNombre(#["Speedy", "Pizzeria Los Hijos de Puta", "Mario el plomero"])
	}
	
	def generarServiciosConNombre(List<String> nombres){
        for(nombre : nombres){
           generarServicioNuevo(nombre)
        }
    }
    
    def generarServicioNuevo(String nombre){
        agregarDato(new Qualifiable(nombre, "servicio", true))
    }
}