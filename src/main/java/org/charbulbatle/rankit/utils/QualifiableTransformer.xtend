package org.charbulbatle.rankit.utils

import org.charbulbatle.rankit.domain.Qualifiable
import org.eclipse.xtend.lib.annotations.Accessors
import java.util.List

class QualifiableTransformer {
	
	def static toFake(Qualifiable quali){
		new SimpleQualifiable() =>[
			nombre = quali.nombre
			tipo = quali.tipo
			id = quali.id
		]
	}
	
	def static transformList(List<Qualifiable> qualis){
		qualis.map[quali | toFake(quali)]
	}
}

@Accessors
class SimpleQualifiable{
	String nombre;
    String tipo;
    int id;
}