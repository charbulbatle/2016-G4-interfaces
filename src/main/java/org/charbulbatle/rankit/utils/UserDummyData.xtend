package org.charbulbatle.rankit.utils

import java.time.LocalDateTime
import java.util.List
import org.charbulbatle.rankit.domain.User
import org.eclipse.xtend.lib.annotations.Accessors

@Accessors
class UserDummyData extends DummyData<User> {
	
	def void generarUsuarioNuevo(String nombre){
		agregarDato(new User(nombre, "1234", LocalDateTime.now, false, false))
	}
	
	def void generarUsuariosConNombre(List<String> nombres){
		for(nombre : nombres){
			generarUsuarioNuevo(nombre)
		}
	}
	
	override generarDatos() {
		generarUsuariosConNombre(#["ADMIN","pepe", "juan", "paula", "leo", "gaby", "lauti"])
	}
	
}