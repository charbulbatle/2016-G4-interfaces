package org.charbulbatle.rankit.utils

import org.eclipse.xtend.lib.annotations.Accessors
import org.charbulbatle.rankit.domain.Qualifiable

@Accessors
class Criteria {
	String nombre
	String tipo
	Integer calificaciones
	Integer ranking
	
	new(){
		
	}
	
	def Boolean matches(Rank rank){
		sameName(rank.calificable) && sameKind(rank.calificable) && minimumQualifications(rank) && minimumRanking(rank)
	}
	
	def private Boolean sameName(SimpleQualifiable qualifiable){
		this.nombre == null || qualifiable.nombre.equals(this.nombre)
	}
	
	def private Boolean sameKind(SimpleQualifiable qualifiable){
		this.tipo == null || qualifiable.tipo.equals(this.tipo)
	}
	
	def private Boolean minimumQualifications(Rank rank){
		this.calificaciones == null || rank.calificaciones >= this.calificaciones
	}
	
	def private Boolean minimumRanking(Rank rank){
		this.ranking == null || this.ranking >=  rank.ranking//No lo puede calcular por si solo salvo que ranking sea una propiedad de Qualifiable
	}
	
}