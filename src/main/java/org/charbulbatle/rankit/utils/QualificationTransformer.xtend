package org.charbulbatle.rankit.utils

import org.charbulbatle.rankit.domain.Qualification
import org.eclipse.xtend.lib.annotations.Accessors
import java.time.format.DateTimeFormatter
import java.time.LocalDateTime
import org.charbulbatle.rankit.domain.repos.QualifiableRepo
import java.util.List

class QualificationTransformer {
	
	def static toReal(SimpleQualification quali){
		// Parsear Fecha
		var DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
		val dateTime = LocalDateTime.parse(quali.fecha, formatter);
	
		new Qualification() => [
			evaluado = quali.evaluado
			puntos = quali.puntos
			fecha = dateTime
			user = quali.user
			detalle = quali.detalle
			tieneContenidoOfensivo = quali.tieneContenidoOfensivo
		]
	}
	
	def static toFake(Qualification quali){
		val DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
		new SimpleQualification() => [
			evaluado 	= quali.evaluado
			tipo 		= QualifiableRepo.instance.getbyName(quali.evaluado).tipo
			puntos 		= quali.puntos
			detalle 	= quali.detalle
			fecha 		= quali.fecha.format(formatter)
		]
	}
	
	def static transformList(List<Qualification> qualis){
		qualis.map[qualification | toFake(qualification)]
	}
	
}
@Accessors
class SimpleQualification{
	String evaluado
	String tipo
	Integer puntos
	String fecha
	String user
	String detalle
	Boolean tieneContenidoOfensivo
}