package org.charbulbatle.rankit.utils

import org.eclipse.xtend.lib.annotations.Accessors
import org.charbulbatle.rankit.domain.Qualifiable

@Accessors
class PlacesDummyData extends ServicesDummyData{
	
	override generarDatos() {
		generarServiciosConNombre(#["Starbucks", "Mc Donalds", "El Charro"])
	}
	
	override generarServicioNuevo(String nombre){
        agregarDato(new Qualifiable(nombre, "lugar", true))
    }
	
}