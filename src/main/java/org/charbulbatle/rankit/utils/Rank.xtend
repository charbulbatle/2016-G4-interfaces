package org.charbulbatle.rankit.utils

import org.eclipse.xtend.lib.annotations.Accessors

@Accessors
class Rank {
	int ranking
	int calificaciones
	SimpleQualifiable calificable
	
}