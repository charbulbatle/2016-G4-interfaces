package org.charbulbatle.rankit.utils

import java.util.ArrayList
import java.util.List
import org.eclipse.xtend.lib.annotations.Accessors

@Accessors
abstract class DummyData<T> {
	private List<T> datos

    new(){
    	super()
        this.datos = new ArrayList<T>
        generarDatos()
    }
    
    def agregarDato(T dato) {
		datos.add(dato)
	}
	
    def abstract void generarDatos()
}