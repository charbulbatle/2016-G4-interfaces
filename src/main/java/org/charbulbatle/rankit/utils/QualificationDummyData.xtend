package org.charbulbatle.rankit.utils
import java.time.LocalDateTime
import java.util.List
import org.charbulbatle.rankit.domain.Qualification
import org.eclipse.xtend.lib.annotations.Accessors
import java.util.ArrayList
import java.util.NoSuchElementException
import org.charbulbatle.rankit.domain.Qualifiable
import java.util.stream.Collectors
import org.charbulbatle.rankit.domain.repos.QualificationRepo

@Accessors
class QualificationDummyData extends DummyData<Qualification>{	
				
	override generarDatos() {
		agregarDato(new Qualification("Pizzeria Los Hijos de Puta", LocalDateTime.now, 10, false, "lauti", ""))
		agregarDato(new Qualification("Speedy", LocalDateTime.now, 0, false, "gaby", ""))
		agregarDato(new Qualification("Mario el plomero", LocalDateTime.now, 7, false, "leo", ""))
		agregarDato(new Qualification("Starbucks", LocalDateTime.now, 9, false, "leo", "El Latte de Dulce de leche es genial! Tambien hay de Cinnamon y Chocolate"))
		agregarDato(new Qualification("El Charro", LocalDateTime.now, 7, false, "ADMIN", "La milanesa mas cara de todo Bernal"))
		agregarDato(new Qualification("Mc Donalds", LocalDateTime.now, 3, false, "lauti", "Me comi el verso"))
	}
    
}