package org.charbulbatle.rankit.utils

import org.eclipse.xtend.lib.annotations.Accessors
import org.charbulbatle.rankit.domain.User

class UserTransformer {
	
	def static User toReal(SimpleUser user){
		new User() => [
			nombre = user.username
			password = user.password
		]
	}
	
	def static IdContainer toID(User user){
		new IdContainer() => [id = user.id]
	}
}

@Accessors
class SimpleUser {
	String username
	String password
}

@Accessors
class IdContainer{
	int id
}