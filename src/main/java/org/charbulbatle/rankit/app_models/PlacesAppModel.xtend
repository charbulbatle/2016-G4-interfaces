package org.charbulbatle.rankit.app_models

import org.charbulbatle.rankit.domain.repos.PlaceRepo

class PlacesAppModel extends ServicesAppModel{
	
	new(){
		super()
        this.repo = PlaceRepo.instance
    }
}