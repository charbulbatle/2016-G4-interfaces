package org.charbulbatle.rankit.app_models

import org.uqbar.commons.model.ObservableUtils
import org.uqbar.commons.utils.Observable
import org.eclipse.xtend.lib.annotations.Accessors

@Accessors
@Observable
class CreateServiceAppModel extends CreatorAppModel<ServicesAppModel> {


	def createService(){
		this.repo.nuevoServicio(name)
	
	}
	
}