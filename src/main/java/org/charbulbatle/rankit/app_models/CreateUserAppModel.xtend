package org.charbulbatle.rankit.app_models

import org.charbulbatle.rankit.domain.repos.UserRepo
import org.eclipse.xtend.lib.annotations.Accessors
import org.uqbar.commons.model.ObservableUtils
import org.uqbar.commons.utils.Observable

@Accessors
@Observable
class CreateUserAppModel extends CreatorAppModel<UserAppModel> {
	
	def createUser(){
		this.repo.nuevoUsuario(this.name)
	}
}