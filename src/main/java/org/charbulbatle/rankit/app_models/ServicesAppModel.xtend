package org.charbulbatle.rankit.app_models

import org.charbulbatle.rankit.domain.Qualifiable
import org.charbulbatle.rankit.domain.repos.ServiceRepo
import org.uqbar.commons.model.ObservableUtils
import java.util.List
import java.util.stream.Collectors
import org.eclipse.xtend.lib.annotations.Accessors
import org.uqbar.commons.utils.Observable
import org.charbulbatle.rankit.utils.Deletable
import org.charbulbatle.rankit.domain.repos.QualificationRepo

@Accessors
@Observable
class ServicesAppModel implements Deletable {
    ServiceRepo repo
    Qualifiable seleccionado
    String buscado

    new(){
        this.repo = ServiceRepo.instance
        this.seleccionado = null
    }
    
    def void setBuscado(String busc){
        this.buscado = busc
        ObservableUtils.firePropertyChanged(this, "servicios")
    }
    
    def isHaySeleccionado(){
		this.seleccionado != null
	}
	def void setSeleccionado(Qualifiable quali){
		seleccionado = quali
		ObservableUtils.firePropertyChanged(this, "seleccionado")
		ObservableUtils.firePropertyChanged(this, "haySeleccionado")
		ObservableUtils.firePropertyChanged(this, "habilitado")
		ObservableUtils.firePropertyChanged(this, "promedio")
		ObservableUtils.firePropertyChanged(this, "calificaciones")
		
	}
    
    def List<Qualifiable> getServicios(){
        repo.getServicios(buscado)
    }
    
    def void setHabilitado(Boolean habilitado){
    	if(seleccionado != null){
    		seleccionado.habilitado = habilitado
    		ObservableUtils.firePropertyChanged(this, "serviciosHabilitados")
    		ObservableUtils.firePropertyChanged(this, "serviciosDeshabilitados")
    	}
    }
    
    def getHabilitado(){
    	if(seleccionado != null){
    		seleccionado.habilitado
    	}else{
    		false
    	}
    }
    
 
    def getPromedio(){
    	if(seleccionado != null){
    		QualificationRepo.instance.calificacionPromedioPara(seleccionado)
    	}else{
    		0
    	}
    }
    
    def getCalificaciones(){
    	if(seleccionado != null){
    		return QualificationRepo.instance.cantCalificacionesPara(seleccionado)
    	}else{
    		return 0
    	}
    }
	
	def nuevoServicio(String servicename) {
		this.repo.generarServicioNuevo(servicename)
		ObservableUtils.firePropertyChanged(this, "servicios")
		ObservableUtils.firePropertyChanged(this, "serviciosInscriptos")
		ObservableUtils.firePropertyChanged(this, "serviciosHabilitados")
		ObservableUtils.firePropertyChanged(this, "serviciosDeshabilitados")
	}
	
	def getServiciosHabilitados(){
		this.repo.cantidadDeInscriptosHabilitados
	}
	
	def getServiciosDeshabilitados(){
		this.repo.cantidadDeInscriptosDeshabilitados
	}
	
	def getServiciosInscriptos(){
		this.repo.cantidadDeInscriptos
	}
	
	override void eliminar(){
		eliminarServicio()
	}
	
	def void eliminarServicio() {
		this.repo.eliminarServicio(seleccionado)
		ObservableUtils.firePropertyChanged(this, "servicios")
		ObservableUtils.firePropertyChanged(this, "serviciosInscriptos")
		ObservableUtils.firePropertyChanged(this, "serviciosHabilitados")
		ObservableUtils.firePropertyChanged(this, "serviciosDeshabilitados")
	}
	
	def getByName(String nombre){
	    repo.getByName(nombre)
	}
}