package org.charbulbatle.rankit.app_models

import java.util.List
import java.util.stream.Collectors
import org.charbulbatle.rankit.domain.User
import org.charbulbatle.rankit.domain.repos.UserRepo
import org.eclipse.xtend.lib.annotations.Accessors
import org.uqbar.commons.model.ObservableUtils
import org.uqbar.commons.utils.Observable
import org.charbulbatle.rankit.utils.Deletable

@Accessors
@Observable
class UserAppModel implements Deletable {
	UserRepo repo
	User seleccionado
	String buscado
	
	new(){
		this.repo = UserRepo.instance
		this.seleccionado = null
	}
	
	def void setBuscado(String busc){
		this.buscado = busc
		ObservableUtils.firePropertyChanged(this, "usuarios")
	}
	
	def List<User> getUsuarios(){
		if(buscado == null){
			return this.repo.getUsuarios()
		} else {
			return this.repo.filtrarPorNombre(buscado)
		}
		
	}
	
	def getBaneado(){
		if(seleccionado != null){
			seleccionado.baneado
		}
		else {
			false
		}
	}
	
	def getActivo(){
		if(seleccionado != null){
			seleccionado.activo
		}
		else {
			false
		}
	}
	
	def void setActivo(Boolean act){
		seleccionado.activo = act
		ObservableUtils.firePropertyChanged(this, "usuariosActivos")
		ObservableUtils.firePropertyChanged(this, "usuariosInactivos")
	}
	
	def getNoBaneado(){
		!baneado
	}
	def isHaySeleccionado(){
		this.seleccionado != null
	}
	
	def void setSeleccionado(User user){
		seleccionado = user
		ObservableUtils.firePropertyChanged(this, "haySeleccionado")
		//Bug de Arena cuando abris una ventana a parte y volves a esta
		ObservableUtils.firePropertyChanged(this, "usuariosBaneados")
		ObservableUtils.firePropertyChanged(this, "usuarios")
		ObservableUtils.firePropertyChanged(this, "baneado")
		ObservableUtils.firePropertyChanged(this, "activo")
		
	}
	
	def getUsuariosRegistrados(){
		this.repo.cantidadDeUsuarios
	}
	
	def getUsuariosActivos(){
		this.repo.cantidadDeUsuariosActivos
	}
	
	def getUsuariosInactivos(){
		this.repo.cantidadDeUsuariosInactivos
	}
	
	def getUsuariosBaneados(){
		this.repo.cantidadDeUsuariosBaneados
	}
	
	def void setBaneado(Boolean banned){
		//seteamos que esta baneado
		seleccionado.setBaneado(banned)
		//avisamos que cambiamos la propiedad
    	ObservableUtils.firePropertyChanged(this, "noBaneado")
    	ObservableUtils.firePropertyChanged(this, "usuariosBaneados")
    	ObservableUtils.firePropertyChanged(this, "usuariosActivos")
    	ObservableUtils.firePropertyChanged(this, "usuariosInactivos")
	}
	
	def void nuevoUsuario(String username){
		this.repo.generarUsuarioNuevo(username)
		ObservableUtils.firePropertyChanged(this, "usuarios")
		ObservableUtils.firePropertyChanged(this, "usuariosRegistrados")
		ObservableUtils.firePropertyChanged(this, "usuariosInactivos")
	}
	
	override eliminar(){
		eliminarUsuario
	}
	
	def void eliminarUsuario() {
		this.repo.eliminarUsuario(seleccionado)
		ObservableUtils.firePropertyChanged(this, "usuarios")
		ObservableUtils.firePropertyChanged(this, "haySeleccionado")
		ObservableUtils.firePropertyChanged(this, "usuariosRegistrados")
		ObservableUtils.firePropertyChanged(this, "usuariosBaneados")
    	ObservableUtils.firePropertyChanged(this, "usuariosActivos")
    	ObservableUtils.firePropertyChanged(this, "usuariosInactivos")
	}
    
    def eliminarUsuarios() {
        this.repo.eliminarUsuarios
    }
	
	def User getByName(String nombre){
	    this.repo.getByName(nombre)
	}
	
	def agregarNuevoUsuario(User nuevoUsuario){
	    this.repo.nuevoUsuario(nuevoUsuario)
	}
	
	
}