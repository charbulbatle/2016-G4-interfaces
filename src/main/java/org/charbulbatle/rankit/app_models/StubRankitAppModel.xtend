package org.charbulbatle.rankit.app_models

import org.charbulbatle.rankit.utils.QualificationDummyData
import org.charbulbatle.rankit.utils.UserDummyData
import org.charbulbatle.rankit.utils.PlacesDummyData
import org.charbulbatle.rankit.utils.ServicesDummyData

class StubRankitAppModel extends RankItAppModel {
	
	new(){
		super()
		
		this.userAppModel.repo.usuarios 				= new UserDummyData().datos
		this.qualificationAppModel.repo.calificaciones 	= new QualificationDummyData().datos
		this.placesAppModel.repo.servicios 				= new PlacesDummyData().datos
		this.serviceAppModel.repo.servicios 			= new ServicesDummyData().datos
	}
}