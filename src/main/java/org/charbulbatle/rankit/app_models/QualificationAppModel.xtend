package org.charbulbatle.rankit.app_models

import java.util.List
import java.util.stream.Collectors
import org.charbulbatle.rankit.domain.Qualification
import org.charbulbatle.rankit.domain.repos.QualificationRepo
import org.eclipse.xtend.lib.annotations.Accessors
import org.uqbar.commons.model.ObservableUtils
import org.uqbar.commons.utils.Observable
import java.time.LocalDateTime
import org.charbulbatle.rankit.domain.repos.UserRepo
import org.charbulbatle.rankit.utils.Deletable

@Accessors
@Observable
class QualificationAppModel implements Deletable {
    QualificationRepo repo
    Qualification seleccionado
    String usuario
    String evaluado
    
    new(String evaluado){
    	this.repo = QualificationRepo.instance
    	this.setEvaluado(evaluado)
    	seleccionado = null
    }
    
    new(){
        this.repo = QualificationRepo.instance
        this.seleccionado = null
    }
    
    def List<Qualification> getCalificaciones(){
    	var List<Qualification> qualifications = this.repo.calificaciones
		
		filtrarPorEvaluado(filtrarPorUsuario(qualifications))
		
	}
	
	def filtrarPorEvaluado(List<Qualification> qualifications) {
		if(evaluado == null){
			return qualifications
		} else {
			return qualifications.stream()
										  .filter(calificacion | calificacion.evaluado.contains(evaluado))
										  .collect(Collectors.toList)
		}
	}
	
	def filtrarPorUsuario(List<Qualification> qualifications) {
		if(usuario == null){
			return qualifications
		} else {
			return qualifications.stream()
										  .filter(calificacion | calificacion.user.contains(usuario))
										  .collect(Collectors.toList)
		}
	}
	
	def isHaySeleccionado(){
		this.seleccionado != null
	}
	
	def isHayUsuario(){
		this.usuario != null
	}
	
	def isHayEvaluado(){
		this.evaluado != null
	}
	
	def getCalificacionesTotales(){
		this.repo.cantidadDeCalificaciones
	}
	
	def getCalificacionesNoOfensivas(){
		this.repo.cantidadDeCalificacionesNoOfensivas
	}
	
	def void setSeleccionado(Qualification qualif){
		seleccionado = qualif
		ObservableUtils.firePropertyChanged(this, "haySeleccionado")
		ObservableUtils.firePropertyChanged(this, "tieneContenidoOfensivo")
	}
	
	def void setEvaluado(String eval){
		evaluado = eval
		ObservableUtils.firePropertyChanged(this, "hayEvaluado")
		ObservableUtils.firePropertyChanged(this, "calificaciones")
		
	}
	
	def void setUsuario(String user){
		usuario = user
		ObservableUtils.firePropertyChanged(this, "hayUsuario")
		ObservableUtils.firePropertyChanged(this, "calificaciones")
		
	}
	
	def agregarCalificacion(String evaluado){
		var calificacion = new Qualification(evaluado, LocalDateTime.now, 5, false, "ADMIN", "Prueba")
		repo.agregarCalificacion(calificacion)
		var usuarito = UserRepo.instance.getByName(calificacion.user)
		usuarito.agregarCalificacion(calificacion)
		ObservableUtils.firePropertyChanged(this, "calificaciones")
		ObservableUtils.firePropertyChanged(this, "calificacionesTotales")
	}
	
	override eliminar(){
		eliminarSeleccionado
	}
	
	def eliminarSeleccionado(){
		repo.eliminar(seleccionado)
		var user = UserRepo.instance.getByName(usuario)
		user.eliminarCalificacion(seleccionado)
		ObservableUtils.firePropertyChanged(this, "calificaciones")
		ObservableUtils.firePropertyChanged(this, "seleccionado")
		ObservableUtils.firePropertyChanged(this, "calificacionesTotales")
	}
	
	def void setTieneContenidoOfensivo(Boolean ofensivo){
		seleccionado.tieneContenidoOfensivo = ofensivo
		//new UserAppModel().marcarComoOfensivo(seleccionado.user)
		var user = UserRepo.instance.getByName(seleccionado.user)
		ObservableUtils.firePropertyChanged(user, "cantidadDeOfensas")
		ObservableUtils.firePropertyChanged(this, "calificacionesOfensivas")
		ObservableUtils.firePropertyChanged(this, "calificacionesNoOfensivas")
		
	}
	
	def getTieneContenidoOfensivo(){
		if(seleccionado != null){
			seleccionado.tieneContenidoOfensivo
		} else{
			false
		}
	}
	
	def getByName(String user, String evaluado){
	    repo.getByUserAndQualifiable(user, evaluado)
	}
	def getCalificacionesOfensivas(){
		this.repo.cantidadDeCalificacionesOfensivas
	}
}