package org.charbulbatle.rankit.app_models

import org.eclipse.xtend.lib.annotations.Accessors
import org.uqbar.commons.utils.Observable

@Accessors
@Observable
class RankItAppModel {
	private UserAppModel userAppModel
	private ServicesAppModel serviceAppModel
	private QualificationAppModel qualificationAppModel
	private PlacesAppModel placesAppModel
	
	new(){
		this.userAppModel = new UserAppModel
		this.serviceAppModel = new ServicesAppModel
		this.qualificationAppModel = new QualificationAppModel
		this.placesAppModel = new PlacesAppModel
	}
	
	def getUserStats(){
		var activos = userAppModel.usuariosActivos.toString
		var totales = userAppModel.usuariosRegistrados.toString
		var baneados = userAppModel.usuariosBaneados.toString
		
		activos + "/" + totales + " (" + baneados + ")"
	}
	
	def getQualificationStats(){
		var totales = qualificationAppModel.calificacionesTotales.toString
		var noOfensivas = qualificationAppModel.calificacionesNoOfensivas.toString
		noOfensivas + "/" + totales
	}	
	
	def getServiceStats(){
		var totales = serviceAppModel.serviciosInscriptos.toString
		var habilitados = serviceAppModel.serviciosHabilitados.toString
		habilitados + "/" + totales
	}	
	
	def getPlaceStats(){
		var totales = placesAppModel.serviciosInscriptos.toString
		var habilitados = placesAppModel.serviciosHabilitados.toString
		habilitados + "/" + totales
	}
}