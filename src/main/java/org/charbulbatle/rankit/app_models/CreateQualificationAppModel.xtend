package org.charbulbatle.rankit.app_models

import java.util.Set
import java.util.stream.Collectors
import org.charbulbatle.rankit.domain.repos.QualifiableRepo
import org.eclipse.xtend.lib.annotations.Accessors
import org.uqbar.commons.model.ObservableUtils
import org.uqbar.commons.utils.Observable

@Accessors
@Observable
class CreateQualificationAppModel extends CreatorAppModel<QualificationAppModel> {
	
	def Set<String> getCalificados(){
		QualifiableRepo.qualifiables.stream.map[ calificable| calificable.nombre].collect(Collectors.toSet)
	}
	
	def createQualification(){
		this.repo.agregarCalificacion(name)
	}

}