package org.charbulbatle.rankit.app_models

import org.uqbar.commons.model.ObservableUtils
import org.eclipse.xtend.lib.annotations.Accessors
import org.uqbar.commons.utils.Observable

@Accessors
@Observable
class CreatorAppModel<AppModel> {
	AppModel repo
	String name
	
	new(AppModel repo){
		this.repo = repo
	}
	
	def boolean isNombreIngresado(){
		name != null && !name.empty
	}
	
	def void setName(String name){
		this.name = name
		ObservableUtils.firePropertyChanged(this, "nombreIngresado")
	}
	
}