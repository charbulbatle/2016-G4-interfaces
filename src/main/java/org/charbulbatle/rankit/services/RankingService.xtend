package org.charbulbatle.rankit.services

import org.charbulbatle.rankit.domain.repos.QualifiableRepo
import org.charbulbatle.rankit.utils.Criteria
import java.util.List
import org.charbulbatle.rankit.utils.Rank
import org.charbulbatle.rankit.domain.Qualifiable
import java.util.ArrayList
import org.charbulbatle.rankit.utils.QualifiableTransformer

class RankingService {
	
	QualifiableRepo qualifiableRepo
	
	new(){
		this.qualifiableRepo = QualifiableRepo.instance
	}
	
	def List<Rank> getRanking(Criteria criteria){
		this.rankQualifiables.filter[calificable | criteria.matches(calificable)].toList
	}
	
	def rankQualifiables(){
		var List<Rank> ranks = new ArrayList()
		var List<Qualifiable> calificables = new ArrayList()
		// Copio los calificables
		calificables.addAll(this.qualifiableRepo.enabledQualifiables);
		// Itero sobre la copia
		var pos = 1;
		while(calificables.size() > 0){
			val i = pos;
			// Obtengo el maximo
			val maximo = calificables.maxBy[calif | calif.ratingPromedio]
			// Lo agrego a la lista de Ranks
			ranks.add(new Rank() =>[
				calificable = QualifiableTransformer.toFake(maximo)
				calificaciones = maximo.calificaciones.size
				ranking = i
			])
			// Lo remuevo de la copia para obtener el siguiente
			calificables.remove(maximo)
			// Aumento la posicion
			pos++;
		}
		return ranks
	}
	
}