package org.charbulbatle.rankit.test

import junit.framework.TestCase
import org.charbulbatle.rankit.app_models.ServicesAppModel
import org.junit.Before
import org.junit.Test
import org.charbulbatle.rankit.utils.ApplicationDummyData

class ServiceAppModelTest extends TestCase{
    
    private ServicesAppModel myServiceAppModel
    
    @Before
    override void setUp(){
    	ApplicationDummyData.dummyEverything
        myServiceAppModel = new ServicesAppModel
        // Se inicializa con tres servicios: [ Speedy, Piezzeria Los Hijos de Puta, Mario el plomero ]
    }
    
    @Test
    def test_Al_iniciarse_cuenta_con_tres_servicios(){
        
        assertEquals(myServiceAppModel.servicios.size, 3)
        assertEquals(myServiceAppModel.servicios.get(0).nombre, "Speedy")
        assertEquals(myServiceAppModel.servicios.get(1).nombre, "Pizzeria Los Hijos de Puta")
        assertEquals(myServiceAppModel.servicios.get(2).nombre, "Mario el plomero")
    }
    
    @Test
    def test_Busco_el_servicio_Mario_el_plomero_y_hay_uno_solo(){
        
        myServiceAppModel.buscado = "Mario el plomero"
        
        assertEquals(myServiceAppModel.servicios.size, 1)
        assertEquals(myServiceAppModel.servicios.get(0).nombre, "Mario el plomero")
    }
    
    @Test
    def test_Se_deshabilita_un_servicio_seleccionado(){
        
        var anQualifiableService = myServiceAppModel.getByName("Speedy")
        
        myServiceAppModel.seleccionado = anQualifiableService
        
        myServiceAppModel.habilitado = false
        
        assertFalse(myServiceAppModel.getByName("Speedy").habilitado)
    }
    
    @Test
    def test_Se_elimina_un_servicio_y_tienen_que_quedar_solo_dos(){
        
        var anQualifiableService = myServiceAppModel.getByName("Pizzeria Los Hijos de Puta")
        
        myServiceAppModel.seleccionado = anQualifiableService
        
        myServiceAppModel.eliminarServicio
        
        assertEquals(myServiceAppModel.servicios.size, 2)
        assertFalse(myServiceAppModel.servicios.contains(anQualifiableService))
    }
  /*  
    @Test
    def test_Se_agrega_un_servicio_y_tienen_que_quedar_solo_tres(){
                           
        myServiceAppModel.nuevoServicio("Remiseria Impuntual")
        
        assertEquals(myServiceAppModel.servicios.size, 4)
        
        var anQualifiableService = myServiceAppModel.getByName("Remiseria Impuntual")
        assertTrue(myServiceAppModel.servicios.contains(anQualifiableService))
    }*/
}