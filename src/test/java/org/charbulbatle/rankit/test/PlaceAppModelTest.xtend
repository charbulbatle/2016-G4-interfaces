package org.charbulbatle.rankit.test

import junit.framework.TestCase
import org.charbulbatle.rankit.app_models.PlacesAppModel
import org.charbulbatle.rankit.domain.repos.PlaceRepo
import org.junit.Before
import org.junit.Test
import org.charbulbatle.rankit.utils.ApplicationDummyData

class PlaceAppModelTest extends TestCase{
    private PlacesAppModel myServiceAppModel
    
    @Before
    override void setUp(){
        myServiceAppModel = new PlacesAppModel()
        // Se inicializa con tres servicios: [Starbucks, Mc Donalds, El Charro]
    }
    
    
    @Test
    def test_Busco_el_servicio_El_Charro_y_hay_uno_solo(){
        
        myServiceAppModel.buscado = "El Charro"
        
        assertEquals(myServiceAppModel.servicios.size, 1)
        assertEquals(myServiceAppModel.servicios.get(0).nombre, "El Charro")
    }
    
    @Test
    def test_Se_deshabilita_un_servicio_seleccionado(){
        
        ApplicationDummyData.dummyEverything
        
        var anQualifiableService = myServiceAppModel.getByName("Starbucks")
        
        myServiceAppModel.seleccionado = anQualifiableService
        
        myServiceAppModel.habilitado = false
        
        assertFalse(myServiceAppModel.getByName("Starbucks").habilitado)
    }
    
    @Test
    def testCuandoSeAgregaUnServicioSeIncrementaLaCantidadDeHabilitadosporUno(){
        
        myServiceAppModel.nuevoServicio("pepita")
        
        assertEquals(myServiceAppModel.serviciosDeshabilitados, 1)
    }
    
    @Test
    def alIniciarLosServiciosDeshabilitadosSonCero(){
    	assertEquals(myServiceAppModel.serviciosDeshabilitados, 0)
    }
    
    @Test
    def alIniciarLosServiciosHabilitadosSonCero(){
    	assertEquals(myServiceAppModel.serviciosHabilitados, 0)
    }
    
    @Test
    def test_Se_elimina_un_servicio_y_tienen_que_quedar_solo_dos(){
        
        var anQualifiableService = myServiceAppModel.getByName("Mc Donalds")
        
        myServiceAppModel.seleccionado = anQualifiableService
        
        assertEquals(myServiceAppModel.serviciosInscriptos, 3)
        
        myServiceAppModel.eliminarServicio
        
        assertEquals(myServiceAppModel.serviciosInscriptos, 2)
        assertFalse(myServiceAppModel.servicios.contains(anQualifiableService))
    }
  
}