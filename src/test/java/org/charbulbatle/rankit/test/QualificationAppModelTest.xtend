package org.charbulbatle.rankit.test

import junit.framework.TestCase
import org.charbulbatle.rankit.app_models.QualificationAppModel
import org.junit.Before
import org.junit.Test
import org.charbulbatle.rankit.utils.ApplicationDummyData

class QualificationAppModelTest extends TestCase{
    
    private QualificationAppModel myQualificationAppModel
    
    @Before
    override void setUp(){
    	ApplicationDummyData.dummyEverything
        myQualificationAppModel = new QualificationAppModel()
        //Se inicializa con: [ Speedy, Pizzeria los Hijos de Puta, Mario el Plomero ]
    }
    
    
    @Test
    def test_Se_filtra_por_Speedy_y_se_encuentra_una_calificacion_solo(){
        
        myQualificationAppModel.evaluado = "Speedy"
        myQualificationAppModel.filtrarPorEvaluado(myQualificationAppModel.getCalificaciones())
        
        assertEquals(myQualificationAppModel.calificaciones.size, 1)
        assertEquals(myQualificationAppModel.calificaciones.get(0).evaluado, "Speedy")
    }
    
    @Test
    def test_Se_filtra_por_usuario_lauti_y_se_encuentra_dos_calificaciones(){
        
        myQualificationAppModel.usuario = "lauti"
        myQualificationAppModel.filtrarPorUsuario(myQualificationAppModel.getCalificaciones())
        
        assertEquals(myQualificationAppModel.calificaciones.size, 2)
        assertEquals(myQualificationAppModel.calificaciones.get(0).user, "lauti")
        assertEquals(myQualificationAppModel.calificaciones.get(0).evaluado, "Pizzeria Los Hijos de Puta")
    }
    
    @Test
    def test_Se_filtra_por_usuario_lauti_y_la_primer_calificacion_tiene_usuario_lauti(){
        
        myQualificationAppModel.usuario = "lauti"
        myQualificationAppModel.filtrarPorUsuario(myQualificationAppModel.getCalificaciones())
        
        assertEquals(myQualificationAppModel.calificaciones.get(0).user, "lauti")
    }
    
    @Test
    def test_Se_filtra_por_usuario_lauti_y_la_primer_calificacion_es_para_la_Pizzeria(){
        
        myQualificationAppModel.usuario = "lauti"
        myQualificationAppModel.filtrarPorUsuario(myQualificationAppModel.getCalificaciones())
        
        assertEquals(myQualificationAppModel.calificaciones.get(0).evaluado, "Pizzeria Los Hijos de Puta")
    }
    
    @Test
    def test_Se_elimina_una_calificacion_y_no_esta_mas_en_las_calificaciones(){
        
        myQualificationAppModel.usuario = "gaby"
        
        var qualificationSpeedy = myQualificationAppModel.getByName("gaby","Speedy")
        
        myQualificationAppModel.seleccionado = qualificationSpeedy
        
        myQualificationAppModel.eliminarSeleccionado
        
        assertEquals(5, myQualificationAppModel.repo.calificaciones.size)
        assertFalse(myQualificationAppModel.calificaciones.contains(qualificationSpeedy))
    }
}
