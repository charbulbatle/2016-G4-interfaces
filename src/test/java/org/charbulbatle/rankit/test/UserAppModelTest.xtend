package org.charbulbatle.rankit.test

import java.time.LocalDateTime
import junit.framework.TestCase
import org.charbulbatle.rankit.app_models.UserAppModel
import org.charbulbatle.rankit.domain.User
import org.junit.Before
import org.junit.Test

class UserAppModelTest extends TestCase{
    
    private UserAppModel myUserAppModel
    private User myUserLeonardo
    private User myUserGabriel

    @Before
    override void setUp(){
       
        myUserAppModel = new UserAppModel()
        myUserLeonardo = new User("Leonardo", "123456", LocalDateTime.now, false, false)
        myUserGabriel = new User("Gabriel", "1234", LocalDateTime.now, false, false)
        myUserAppModel.eliminarUsuarios()
    }
    
    @Test
    def test_Se_registran_dos_usuarios_nuevos_y_quedan_guardados_en_el_sistema(){
        
        assertEquals(myUserAppModel.usuarios.size, 0)
        
        myUserAppModel.agregarNuevoUsuario(myUserLeonardo)
        myUserAppModel.agregarNuevoUsuario(myUserGabriel)        
        assertEquals(myUserAppModel.usuarios.size, 2)
        
        myUserAppModel.setSeleccionado(myUserLeonardo)
        assertEquals(myUserAppModel.seleccionado, myUserLeonardo)
        assertEquals(myUserAppModel.seleccionado.nombre,"Leonardo" )
        assertEquals(myUserAppModel.seleccionado.password, "123456")
        
        myUserAppModel.setSeleccionado(myUserGabriel)
        assertEquals(myUserAppModel.seleccionado, myUserGabriel)
        assertEquals(myUserAppModel.seleccionado.nombre,"Gabriel" )
        assertEquals(myUserAppModel.seleccionado.password, "1234")
    }
    
    @Test
    def test_Al_iniciar_ningun_usuario_esta_activo(){
        
        myUserAppModel.agregarNuevoUsuario(myUserLeonardo)
        myUserAppModel.agregarNuevoUsuario(myUserGabriel)
        
        myUserAppModel.setSeleccionado(myUserLeonardo)
        assertFalse(myUserAppModel.seleccionado.activo)
        
        myUserAppModel.setSeleccionado(myUserGabriel)
        assertFalse(myUserAppModel.seleccionado.activo)
    }

    @Test
    def test_Al_iniciar_ningun_usuario_esta_baneado(){
        
        
        //new al usuario
        //assert sobre usuario si esta baneado
        myUserAppModel.agregarNuevoUsuario(myUserLeonardo)
        myUserAppModel.agregarNuevoUsuario(myUserGabriel)
        
        myUserAppModel.setSeleccionado(myUserLeonardo)
        assertFalse(myUserAppModel.seleccionado.baneado)
        
        myUserAppModel.setSeleccionado(myUserGabriel)
        assertFalse(myUserAppModel.seleccionado.baneado)
    }

    
    @Test 
    def test_Un_usuario_al_ser_baneado_pasa_a_estar_inactivo(){
        
        myUserAppModel.agregarNuevoUsuario(myUserLeonardo)
        myUserAppModel.agregarNuevoUsuario(myUserGabriel)
        
        myUserAppModel.setSeleccionado(myUserGabriel)
        
        myUserAppModel.setBaneado(true)
        
        assertFalse(myUserGabriel.activo)
    }
}