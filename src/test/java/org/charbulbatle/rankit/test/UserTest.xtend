package org.charbulbatle.rankit.test

import org.charbulbatle.rankit.domain.User
import org.junit.Test
import java.time.LocalDateTime
import org.junit.Before
import org.charbulbatle.rankit.domain.Qualification

import static org.junit.Assert.*

class UserTest {
	User myUserLeonardo
	User myUserGabriel
	Qualification calificacion
	
	@Before
	def void setUp(){
		myUserLeonardo = new User("Leonardo", "123456", LocalDateTime.now, false, false)
        myUserGabriel = new User("Gabriel", "1234", LocalDateTime.now, false, false)
        calificacion = new Qualification("Starbucks", LocalDateTime.now, 9, true, "leo", "El Latte de Dulce de leche es genial! Tambien hay de Cinnamon y Chocolate")
	}
	
	@Test
    def test_un_usuario_nuevo_no_esta_baneado(){
       
        assertFalse(myUserLeonardo.baneado)
        
    }
	
    @Test
    def test_Al_realizar_mas_de_cinco_publicaciones_ofensivas_un_usuario_queda_baneado(){
        
        //Agregar 6 calificaciones deberia banearlo
        myUserLeonardo.agregarCalificacion(calificacion)
        myUserLeonardo.agregarCalificacion(calificacion)
        myUserLeonardo.agregarCalificacion(calificacion)
        myUserLeonardo.agregarCalificacion(calificacion)
        myUserLeonardo.agregarCalificacion(calificacion)
        myUserLeonardo.agregarCalificacion(calificacion)
        
        assertTrue(myUserLeonardo.baneado)
        
    }
    
    @Test
    def test_Al_realizar_cinco_publicaciones_ofensivas_un_usuario_NO_queda_baneado(){
        
        //Agregar 5 calificaciones NO deberia banearlo
        myUserLeonardo.agregarCalificacion(calificacion)
        myUserLeonardo.agregarCalificacion(calificacion)
        myUserLeonardo.agregarCalificacion(calificacion)
        myUserLeonardo.agregarCalificacion(calificacion)
        myUserLeonardo.agregarCalificacion(calificacion)
        
        assertFalse(myUserLeonardo.baneado)
        
    }
}